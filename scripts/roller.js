class SR3DiceRoller {

    static async Init(controls, html) {

        // Render the chat card template

        const templateData = {
            //tokenId: token ? `${token.scene._id}.${token.id}` : null,
            //img: hasDataToken ? data.token.img : this.img,
            //name: hasDataToken ? data.token.name : this.name,
            titleText: 'hi',
        };

        const template = `modules/sr3-dice-roller/templates/sr3-dr.html`;
        const htmlform = await renderTemplate(template, templateData);

        html.append(htmlform);

        document.getElementById("toggleButton").addEventListener("click", ev => this.PopupSheet(ev, html));
        html.find('.dice-calculator__roll').on('click', event => {
            event.preventDefault();
            let $num_dice_input = html.find('.num-dice-input');
            let num_dice = Number($num_dice_input.val());

            let  $tn_input = html.find('.tn-input');
            let tn = Number($tn_input.val());


             console.log(num_dice);
             console.log(tn);
             let dice_vals = process_roll(event, html, num_dice, tn);
             this.create_roll_message(event, html, num_dice, tn, dice_vals);
             //roll_dist(event, html, num_dice, tn);

 });

        //document.getElementById("rollForm").onsubmit = ev => this.submitRoll(ev, html);
        //htmlform[0].addEventListener('click', ev => this.PopupSheet(ev, html));

        //this._createDiceTable(html);
    }


    static async PopupSheet(event, html) {
        //console.log("SDR | clicked");
        //canvas.stage.children.filter(layer => layer._active).forEach(layer => layer.deactivate());
        if (html.find('.sr3dr-scene-control').hasClass('active')) {
            this._close(event, html);
        } else {
            this._open(event, html);
        }
    }

    static async _close(event, html) {
        //console.log("SDR | closed");
        html.find('#SDRpopup').hide();
        html.find('.sr3dr-scene-control').removeClass('active');
        html.find('.scene-control').first().addClass('active');

        event.stopPropagation();
    }

    static async _open(event, html) {
        //console.log("SDR | opened");
        //this._createDiceTable(html);
        html.find('.scene-control').removeClass('active');
        html.find('#SDRpopup').show();
        html.find('.sr3dr-scene-control').addClass('active');
        event.stopPropagation();
    }


    static async create_roll_message(event, html, num_dice, tn, dice_vals)
    {

        let data_arr = [];
        let i;
        let numSuccesses = 0;
        for (i = 0; i < num_dice; i++)
        {
            if (dice_vals[i] >= tn)
            {
                let dice_data =
                {
                    diceVal: String(dice_vals[i]),
                    isSuccess: true
                }
                data_arr.push(dice_data);
                numSuccesses+=1
                console.log(dice_data);
            }
            else
            {
                let dice_data =
                {
                    diceVal: String(dice_vals[i]),
                    isSuccess: false
                }
                data_arr.push(dice_data);
                console.log(dice_data);
            }


        }
        const template = `modules/sr3-dice-roller/templates/sr3-roll-message.html`;
        const templateData = {
            numDice: String(num_dice),
            tN: String(tn),
            diceData: data_arr,
            numSuccesses: numSuccesses
        };

        //const htmlform = await renderTemplate(template, templateData);
        const htmlform = await renderTemplate(template, templateData);




        ChatMessage.create({
        user: game.user._id,
        content: htmlform
        });

        //return htmlform;
    }


}

function process_roll(event, html, num_dice, tn) {


       let final_vals = [];


       let i;
       for (i = 0; i < num_dice; i++) {
           let formula = 1 + "d" + 6;
           let r = new Roll(formula);
           r.evaluate();
           //console.log(r.dice);
          // console.log(r.results);

           let d_val = r.results[0];
           let total_d_val = d_val;
           console.log("roll" + i + ": " + d_val);
           while (d_val == 6)
           {

               let explode = new Roll(formula);
               explode.evaluate();
               d_val = explode.results[0];
               console.log("roll" + i + "_explode: " + total_d_val + " + " + d_val + " = " + (total_d_val + d_val));
               total_d_val += d_val;

           }

           final_vals.push(total_d_val);
           //figure out what the exploding dice values are.
       }

       console.log(final_vals);

       let another_val;
       let successes = 0;
       let j;
       for (j = 0; j < num_dice; j++)
       {
           another_val = final_vals[j];
           console.log("is " + another_val + " greater than or equal to " + tn + "?")
           if (another_val >= tn)
           {
               console.log("yes");
               successes +=1;
           }
           else {
               console.log("no");
           }
       }

       //let dice_val_concat = ``;
       //for (vall in final_vals)
      // {
        //   let d_v = ` ` + String(vall) + ` `;
           //console.log(d_v);
          // console.log(dice_val_concat);
          // dice_val_concat.concat(d_v);
      // }



       //let results_html = `roll ${num_dice}d6 versus target number ${tn} <br> ${final_vals.toString()} <br> successes: ${successes}.`
       //let results_html = create_roll_message(event, html, num_dice, tn)




       //r.toMessage({
        //   user: game.user._id,
      // })

       SR3DiceRoller._close(event, html);
       return final_vals;
}


function roll_dist(event, html, num_dice, tn) {


       let counts = [0,0,0,0,0,0];
       let num_rolls = 1000000;
       let i;

       for (i = 0; i < num_rolls; i++) {
           let formula = 1 + "d" + 6;
           let r = new Roll(formula);
           r.evaluate();
           //console.log(r.dice);
          // console.log(r.results);

           let d_val = r.results[0];

           counts[d_val -1] += 1;
           //figure out what the exploding dice values are.
       }

       console.log(counts);

       let final_vals = [0,0,0,0,0,0];
       for (i = 0; i < 6; i++)
       {
           final_vals[i] = counts[i] / num_rolls;
       }


       let results_html = `counts: <br>1: ${counts[0]}<br>2: ${counts[1]}<br>3: ${counts[2]}<br>4: ${counts[3]}<br>5: ${counts[4]}<br>6: ${counts[5]}<br>`;


       ChatMessage.create({
       user: game.user._id,
       content: results_html
       });


       //r.toMessage({
        //   user: game.user._id,
      // })

       SR3DiceRoller._close(event, html);

}

const preloadTemplates = () => {
  const templatePaths = [
    `modules/sr3-dice-roller/templates/sr3-dr.html`,
  ];
  return loadTemplates(templatePaths);
};

Hooks.on('renderSceneControls', (controls, html) => { SR3DiceRoller.Init(controls, html); });

Hooks.on('ready', () => {
    // Replace with the selected die formula.
// $(document).on('click', '.dice-calculator__roll', function(event) {
 //  let formula = $(this).text();
 //  $num_dice = html.find('.num-dice-input');
 //  num_dice = Number($num_dice.val());

 //  $tn = html.find('.tn-input');
  // tn = Number($tn.val());


 //  console.log(num_dice);
 //  console.log(tn);
   //$('.dice-calculator__text-input').val(formula);
// });

});

Hooks.once("init", () => {
	game.settings.register("sr3-dice-roller", "maxDiceCount", {
		name: game.i18n.localize("SR3DiceRoller.maxDiceCount.name"),
		hint: game.i18n.localize("SR3DiceRoller.maxDiceCount.hint"),
		scope: "world",
		config: true,
		default: 8,
		type: Number
	});
  preloadTemplates();
  console.log("!!!!SR3 dice roller loaded! Yay!");

});
